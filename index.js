
const FIRST_NAME = "Andrei";
const LAST_NAME = "Cazacu";
const GRUPA = "1090";

class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails() {
        return this.name + " " + this.surname + " " + this.salary;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name, surname, salary, experience = 'JUNIOR') {
       super(name, surname, salary);
       this.experience = experience.toUpperCase();
   }

   applyBonus() {
       switch(this.experience) {
           case 'JUNIOR': 
               return this.salary * 1.1;
           case 'MIDDLE':
               return this.salary * 1.15;
           case 'SENIOR':
               return this.salary * 1.2;
           default:
               return this.salary * 1.1;
       }
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}